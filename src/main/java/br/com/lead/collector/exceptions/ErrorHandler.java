package br.com.lead.collector.exceptions;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
//    @ResponseStatus(code = )
    public HashMap<String, String> manipulacadoDeErrosDeValidacao(MethodArgumentNotValidException exception){
        return null;
    }

}
