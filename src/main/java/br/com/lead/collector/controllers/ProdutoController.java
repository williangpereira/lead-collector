package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto registrarProduto(@RequestBody Produto produto) {
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> listarProduto() {
        Iterable<Produto> produtos = produtoService.buscarTodosProdutos();
        return produtos;
    }

    @GetMapping("/id/{id}")
    public Produto bucarPorId(@PathVariable(name = "id") Integer id) {
        try {
            Produto produto = produtoService.encontrarPorId(id);
            return produto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/nome/{nome}")
    public Produto buscarPorNome(@PathVariable(name = "nome") String nome) {
        Produto produtoNome = produtoService.buscarProdutoPorNome(nome);
        return produtoNome;
    }
}



