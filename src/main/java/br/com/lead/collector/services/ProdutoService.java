package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodosProdutos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Produto encontrarPorId(Integer id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        return optionalProduto.get();
    }

    public Produto buscarProdutoPorNome(String nome){
        Optional<Produto> produtoNome = produtoRepository.findByNome(nome);
        return produtoNome.get();
    }

}
